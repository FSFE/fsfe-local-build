#!/bin/bash
# =============================================================================
# Start a build of the fsfe-website checkout in the current working directory
# =============================================================================
# SPDX-FileCopyrightText: © 2020, 2021 Reinhard Müller <reinhard@fsfe.org>
# SPDX-License-Identifier: WTFPL
# =============================================================================

set -e

# Find out fsfe-website directory
gitdir="$(git rev-parse --show-toplevel)"

# Find out fsfe-local-build directory
builddir="$(dirname "$(readlink -f "$0")")"

# Start the build
echo "Building from ${gitdir} into ${builddir}/fsfe.org"
case "$1" in
  --full|-f)
    "${gitdir}/build/build_main.sh" build_into "${builddir}/fsfe.org" --statusdir "${builddir}/fsfe.org/status.fsfe.org/fsfe.org"
    ;;
  *)
    "${gitdir}/build/build_main.sh" build_run "${builddir}/fsfe.org" --statusdir "${builddir}/fsfe.org/status.fsfe.org/fsfe.org"
    ;;
esac
