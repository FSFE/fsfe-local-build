#!/usr/bin/env bash
########################################################################
#  Copyright (C) 2016 Max Mehl <max.mehl@fsfe.org>
########################################################################
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  
########################################################################
#  
#  A script to preview XHTML files locally, best to use with FSFE's 
#  website setup.
#  
#  Instructions: https://blog.mehl.mx/2016/build-fsfe-websites-locally/
#  
#######################################################################

# Coloured and tagged output
function echo_ERR {
  echo -e '\033[0;31m'"[ERROR] $1"'\033[0m'
}
function echo_SUC {
  echo -e '\033[0;32m'"[SUCCESS] $1"'\033[0m'
}
function echo_WARN {
  echo -e '\033[0;33m'"[WARNING] $1"'\033[0m'
}
function echo_INFO {
  echo -e '\033[0;37m'"[INFO] $1"'\033[0m'
}

# Check dependencies (stolen from build_main.sh)
deperrors=''
for depend in realpath rsync xsltproc xmllint sed find egrep grep wc make tee date iconv; do
  if ! which "$depend" >/dev/null 2>&1; then
    deperrors="$depend $deperrors"
  fi
done
if [ -n "$deperrors" ]; then
  printf '\033[1;31m'
  cat <<-EOF
	The build script depends on some other programs to function.
	Not all of those programs could be located on your system.
	Please use your package manager to install the following programs:
	EOF
  printf '\n\033[0;31m%s\n' "$deperrors"
  exit 1
fi >>/dev/stderr

ROOT=$(dirname "$(readlink -f "$0")")
if [ ! -e "$ROOT"/config.cfg ]; then echo "Missing config.cfg file. Edit and rename config.cfg.sample"; exit 1; fi
source "$ROOT"/config.cfg

# Help listing
if [ "$1" = "" ]; then
  self=$(basename $0) 
  echo_WARN "No parameters or variables given!"
  echo
  echo "Usage: "
  echo
  echo "$self file.en.xhtml"
  echo "   -- build single XHTML file from your source to the local web "
  echo "      directory"
  echo "      The destination will be on the same relative level of the "
  echo "      selected XHTML file"
  echo "      If not a XHTML file, it will be copied to the web directory"
  echo
  echo "$self --copy picture.png look/style.css"
  echo "   -- Just copies several files to the local web directory without "
  echo "      even trying to build them"
  exit 0
fi

# Remove trailing slashes
LOC_trunk=$(echo $LOC_trunk | sed 's|/$||')
LOC_out=$(echo $LOC_out | sed 's|/$||')
LOC_trunk_dev=$(echo $LOC_trunk_dev | sed 's|/$||')

# Pure copy
if [ "$1" = "--copy" ]; then
  numargs=$(($# - 1))
  echo_INFO "Starting a plain mass copy with $numargs file(s)..."
  for ((i=1; i <= $numargs; i++)); do
    SRC_full=$(realpath $2)
    DST_full=$(echo $SRC_full | sed -E "s|$LOC_trunk|$LOC_out|")
    echo_INFO "Copying $SRC_full to $DST_full..."
    if [ -d "$SRC_full" ] && [ -e "$DST_full" ]; then
      rm -rf "$DST_full"
      cp -R $SRC_full $DST_full
    else
      cp -R $SRC_full $DST_full
    fi
    
    shift
  done
  echo_SUC "Copying finished. Files should be visible via the local webserver now."
  exit 0
fi

# Run through number of files given as arguments
numargs=$#
echo_INFO "Building $numargs file(s)"
if [ $numargs -gt 1 ]; then
  echo 
fi
for ((i=1; i <= $numargs; i++)); do
  SRC_rel=$1  # e.g. index.en.xhtml
  SRC_full=$(realpath $SRC_rel) # Full path to source file

  # CHECK whether the source file is located in LOC_trunk
  if [ ! $(echo $SRC_full | grep "$LOC_trunk") ]; then
    echo_ERR "Given source file is not part of \$LOC_trunk (currently set to \"$LOC_trunk\"). Please only build files which reside in or below this directory. Aborting now."
    exit 1
  fi

  # CHECK whether the source file is a directory (only --copy allowed)
  if [ -d "$SRC_full" ]; then
    echo_ERR "The given file \"$SRC_full\" is a directory. Those can only be copied by using --copy as a parameter."
    echo_ERR "NOTE: Using --copy won't build potential .xhtml files inside a directory. If you have this case, please first copy an empty directory and then build the .xhtml files inside them."
    exit 1
  fi

  # TEST if a DEV Git directory is used. If yes, copy clean file to this Dir
  if [ "$LOC_trunk_dev" != "" ]; then
    SRC_full_dev=$(echo $SRC_full | sed -E "s|$LOC_trunk|$LOC_trunk_dev|")
    echo_INFO "A \"dirty\" Development Git directory ($LOC_trunk_dev) is being used. Copy file from clean Git directory ($LOC_trunk) to Development directory..."
    cp $SRC_full $SRC_full_dev  # copy file from clean Git to dev/dirty Git
    SRC_full=$SRC_full_dev
    LOC_trunk=$LOC_trunk_dev
  fi

  echo_INFO "Using file $SRC_full as source..."

  ### TRANSFORM XHTML file path
  DST_full=$(echo $SRC_full | sed -E "s|$LOC_trunk|$LOC_out|")  # replace Git directory by build/HTTP destination

  ### Create destination directory if necessary
  DST_full_dir="$(dirname $DST_full)"
  if [ ! -e "$DST_full_dir" ]; then
    echo_INFO "Creating destination directory $DST_full_dir"
    mkdir -p "$DST_full_dir"
  fi

  ### BUILD/COPY FILE depending on file extension
  ftype=${SRC_full##*.} # get extension of file
  ftype=$(echo "$ftype" | tr '[:upper:]' '[:lower:]')
  ftype_build="xhtml" # file types which have to be built

  if [[ "$ftype" == @($ftype_build) ]]; then  # XHTML file
    DST_full=$(echo $DST_full | sed -E "s/$ftype_build/html/")  # Replace xhtml by html
    echo_INFO "XHTML file detected. Going to build into $DST_full ..."
    
    xmllint --noout "$SRC_full" || (echo; echo "Syntax error in $SRC_full. Error message above. Please fix!"; exit 1)
    if [[ "$?" != "0" ]]; then exit 1; fi
    
    $LOC_trunk/build/build_main.sh process_file $SRC_full > $DST_full

  else # just copy file
    echo_INFO "File type ($ftype) is detected as not to be built. Just linking it to $DST_full ..."
    ln -sf $SRC_full $DST_full
  fi


  ### START WEBSERVER if necessary
  if [ "$HTTPD" == "python" ]; then   # python
    if [ ! $(pgrep -f "pywebserver-fsfe.py $LOC_out") ]; then
      echo_INFO "Starting $HTTPD webserver"
      nohup "$ROOT/pywebserver-fsfe.py" "$LOC_out" "$HTTPD_port" > "$ROOT"/pywebserver-fsfe.log 2>&1 &
    else
      echo_INFO "$HTTPD webserver already seems to be running."
    fi
  elif [ "$HTTPD" == "lighttpd" ]; then
    if [ ! $(pgrep -f "lighttpd.*${HTTPD_conf}") ]; then   # lighttpd
      echo_INFO "Starting $HTTPD webserver"
      /usr/sbin/lighttpd -f "$HTTPD_conf"
    else
      echo_INFO "$HTTPD webserver already seems to be running."
    fi
  elif [ "$HTTPD" == "" ]; then   # empty variable
    echo_ERR "No webserver specified. Please define variable HTTPD in config.cfg. Aborting now."
    exit 1
  else    # something custom
    echo_WARN "Unknown webserver defined. Probably you won't be able to see the file in your browser. Building it anyway. Please check variable \"HTTPD\" in config.cfg." 
  fi

  ### SHOW RESULTS
  DST_rel=$(echo $DST_full | sed "s|$LOC_out||") # relative path
  echo_SUC "Finished. File can be viewed at http://localhost:$HTTPD_port$DST_rel"
  
  if [ $numargs -gt 1 ]; then
  echo "++++++++++++++++++++++++++++++++++"
  fi
  
  shift
done

if [ $numargs -gt 1 ]; then
  echo 
  echo_SUC "$numargs files have been processed. Preview links to each file can be found individually above."
fi
