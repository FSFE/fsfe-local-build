FROM alpine:3.9

RUN apk add --no-cache \
    bash \
    bash-completion \
    coreutils \
    diffutils \
    findutils \
    inotify-tools \
    libxml2-utils \
    libxslt \
    make \
    procps \
    python3 \
    rsync

RUN mkdir -p /fsfe-local-build/fsfe.org

EXPOSE 8000

CMD sh /fsfe-local-build/docker-cmd.sh

