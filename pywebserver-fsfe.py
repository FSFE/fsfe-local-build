#!/usr/bin/env python3

import http.server
import socketserver
import os
import sys

WEB_DIR = sys.argv[1]
WEB_PORT = int(sys.argv[2])

os.chdir(WEB_DIR)

Handler = http.server.SimpleHTTPRequestHandler
httpd = socketserver.TCPServer(("", WEB_PORT), Handler)
httpd.serve_forever()
