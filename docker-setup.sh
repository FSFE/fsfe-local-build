#!/usr/bin/env bash
########################################################################
#  Copyright (C) 2019 Michael Weimann <mweimann@fsfe.org>
########################################################################
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  
########################################################################
#  
#  This script runs the docker dev setup.
#  
#######################################################################

echo -e "\e[96m→ Cloning the website repo\e[0m"
git clone git@git.fsfe.org:FSFE/fsfe-website.git
echo ""

echo -e "\e[96m→ Setting up the config file\e[0m"
cp config.cfg.docker config.cfg
echo ""

echo -e "\e[96m→ Linking the look directory\e[0m"
ln -s ../fsfe-website/look fsfe.org/look
echo ""

echo -e "\e[96m→ Starting the container\e[0m"
docker-compose up --build -d
echo ""

echo -e "\e[96m→ Initial full build\e[0m"
echo -e "\e[33m⚠ This may take some hours\e[0m"
docker exec fsfe-local-build \
 bash /fsfe-local-build/fsfe-website/build/build_main.sh \
 build_into /fsfe-local-build/fsfe.org/ \
 --statusdir /fsfe-local-build/status/ \
 || true
echo ""

echo -e "\e[96m→ Stopping the container\e[0m"
docker-compose stop
echo ""

