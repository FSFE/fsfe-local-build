# FSFE Website Local Build

This is a collection of scripts, tools and configuration files necessary to build fsfe.org websites locally. 

Full instructions can be found in FSFE's wiki: https://wiki.fsfe.org/TechDocs/Mainpage/BuildLocally

## Docker based development environment

For convenience local development you may want to use the docker contaienr provided by this repo.

The container contains everything you need to work on the website including serving a preview, building the static pages and the CSS files.

### Requirements

* Docker https://docs.docker.com/install/
* Docker Compose https://docs.docker.com/compose/install/
* Patience (for the website build)

### Website Dev Instructions

#### Initial set up

Just run the following command for the initial setup:

```
./docker-setup.sh
```

⚠ The command may run some hours, since it does the initial full build of the website.

#### Development

Spin up the container (⚠ takes some minutes):

    docker-compose up

The website should now be available on your machine under http://127.0.0.1:8000/.

Build a single page after changes (e.g. `index.de.xhtml`):

```
docker exec \
    --workdir /fsfe-local-build/fsfe.org \
    fsfe-local-build \
    bash ../fsfe-preview.sh ../fsfe-website/index.de.xhtml
```

Style modifications in `fsfe-website/look` trigger a re build of the styles.
After modifications a page reload shoud show the changes.
